import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) =>
      MaterialApp(
          home: Scaffold(
            body: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(30),
              child: ClipPath(
                clipper: MyCustomClipper(),
                child: Image(
                  image: NetworkImage(
                      'https://media.istockphoto.com/id/178149253/es/foto/espacio-profundo-de-fondo.jpg?s=1024x1024&w=is&k=20&c=modkY5TPZFMu60PaRF6-XHW3AX0-EapH8t5eroc0YrA='
                  ),
                ),
              ),
            ),
          ),
      );
}

class MyCustomClipper extends CustomClipper<Path>{

  @override
  Path getClip(Size size){
      Path path = Path()
          ..lineTo(0, 0)
          ..lineTo(0, size.height)
          ..lineTo(size.width, size.height * .5)
          ..lineTo(size.width, size.height * .5);

      return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}


